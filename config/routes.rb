Rails.application.routes.draw do

  root 'index#index'

  match '/catalog', to: "index#catalog", via: "get"
  match '/school_card', to: "index#school_card", via: "get"
  match '/contacts', to: "index#contacts", via: "get"
  match '/questions', to: "index#questions", via: "get"
  match '/cities', to: "index#cities", via: "get"
  match '/reviews', to: "index#reviews", via: "get"
  match '/landing', to: "index#landing", via: "get"


end

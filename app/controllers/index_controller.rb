class IndexController < ApplicationController

  def index
  end

  def catalog
  end

  def school_card
  end

  def contacts
  end

  def questions
  end

  def cities
  end

  def reviews
  end

end
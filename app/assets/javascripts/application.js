// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require jquery-ui
//= require jquery_ujs
//= require turbolinks
//= require_tree .

$(function(){
    initializeSearchForm();
    initializeFeedbackForm();
    initializeMobileMenu();
    initializeCatalogFilter();
    initializePriceSlider();
    initializeCatalogNavigation();
    initializeSchoolTable();
    initializePopup();
    initPhotoSlider("#photo_slider");
    initializeQuestionForm();
    initializeLocation();
    initializeMobileKursForm();
    initMobileSlider();
    initializeCustomSelect();
    initializePrahaMap();
    initializeKursForm();
    initializeFakeSelect();
    initializeMRequestForm();
    initializeMQuestForm();
    initializeQuestionsBar();
    initializeQuestionsPlanks();
   //initPhotoSlider("#mobile_photo_slider");
});

function initializeSearchForm() {

    searchForm = $("#search_form");
    searchTextField = $(searchForm.find("input[type=text]"));
    searchSubmitField = $(searchForm.find("input[type=submit]"));
    placeholderClass = ".placeholder";

    searchTextField.focusin(function(){
        $(this).prev(placeholderClass).hide();
        $(this).next().show();
    });
    searchTextField.focusout(function(){
        if ($(this).val() == "")
            $(this).prev(placeholderClass).show();
        $(this).next().hide();
    });
    searchTextField.prev(placeholderClass).click(function(){
        $(this).focus();
    });

}

function initializePrahaMap() {
    $("#ten_position").hover(function(){
        $("#p_map").attr("src", "images/maps/mapa10.png");
    });
    $("#ten_position").mouseleave(function(){
        $("#p_map").attr("src", "images/maps/mapa0.png");
    });
    $("#nine_position").hover(function(){
        $("#p_map").attr("src", "images/maps/mapa9.png");
    });
    $("#nine_position").mouseleave(function(){
        $("#p_map").attr("src", "images/maps/mapa0.png");
    });
    $("#eight_position").hover(function(){
        $("#p_map").attr("src", "images/maps/mapa8.png");
    });
    $("#eight_position").mouseleave(function(){
        $("#p_map").attr("src", "images/maps/mapa0.png");
    });
    $("#seven_position").hover(function(){
        $("#p_map").attr("src", "images/maps/mapa7.png");
    });
    $("#seven_position").mouseleave(function(){
        $("#p_map").attr("src", "images/maps/mapa0.png");
    });
    $("#six_position").hover(function(){
        $("#p_map").attr("src", "images/maps/mapa6.png");
    });
    $("#six_position").mouseleave(function(){
        $("#p_map").attr("src", "images/maps/mapa0.png");
    });
    $("#five_position").hover(function(){
        $("#p_map").attr("src", "images/maps/mapa5.png");
    });
    $("#five_position").mouseleave(function(){
        $("#p_map").attr("src", "images/maps/mapa0.png");
    });
    $("#four_position").hover(function(){
        $("#p_map").attr("src", "images/maps/mapa4.png");
    });
    $("#four_position").mouseleave(function(){
        $("#p_map").attr("src", "images/maps/mapa0.png");
    });
    $("#third_position").hover(function(){
        $("#p_map").attr("src", "images/maps/mapa3.png");
    });
    $("#third_position").mouseleave(function(){
        $("#p_map").attr("src", "images/maps/mapa0.png");
    });
    $("#second_position").hover(function(){
        $("#p_map").attr("src", "images/maps/mapa2.png");
    });
    $("#second_position").mouseleave(function(){
        $("#p_map").attr("src", "images/maps/mapa0.png");
    });
    $("#first_position").hover(function(){
        $("#p_map").attr("src", "images/maps/mapa1.png");
    });
    $("#first_position").mouseleave(function(){
        $("#p_map").attr("src", "images/maps/mapa0.png");
    });
}

function initializeFakeSelect() {
    $("body").click(function(){
        $(".select_field ul.items").hide();
    });
    $(".select_field .fake_select_box").click(function(e){
        $(".select_field ul.items").hide();
        $(this).next().show();
        e.stopPropagation();
    });
    $(".select_field ul.items li").click(function(e) {
        $(this).parent().prev().find(".label").text($(this).text());
        $(this).parent().hide();
        e.stopPropagation();
    });
}

function initializeCustomSelect() {
    /*
    $.widget( "custom.iconselectmenu", $.ui.selectmenu, {
        _renderItem: function( ul, item ) {
            var li = $( "<li>", { text: item.label } );

            if ( item.disabled ) {
                li.addClass( "ui-state-disabled" );
            }

            $( "<span>", {
                style: item.element.attr( "data-style" ),
                "class": "ui-icon " + item.element.attr( "data-class" )
            })
                .appendTo( li );

            return li.appendTo( ul );
        }
    });

    $( "#stage_field" )
        .iconselectmenu()
        .iconselectmenu( "menuWidget" )
        .addClass( "ui-menu-icons customicons" );
    */
}

function initializeKursForm() {

    form = $("#kurs_popup");
    textField = $(form.find("input[type=text]"));
    placeholderClass = ".placeholder";

    textField.focusin(function(){
        $(this).prev(placeholderClass).hide();
        $(this).next().show();
    });
    textField.focusout(function(){
        if ($(this).val() == "")
            $(this).prev(placeholderClass).show();
        $(this).next().hide();
    });
    textField.prev(placeholderClass).click(function(){
        $(this).focus();
    });

}

function initializeMobileKursForm() {

    form = $("#mobile_kurs");
    textField = $(form.find("input[type=text]"));
    placeholderClass = ".placeholder";

    textField.focusin(function(){
        $(this).prev(placeholderClass).hide();
        $(this).next().show();
    });
    textField.focusout(function(){
        if ($(this).val() == "")
            $(this).prev(placeholderClass).show();
        $(this).next().hide();
    });
    textField.prev(placeholderClass).click(function(){
        $(this).focus();
    });

}

function initializeFeedbackForm() {

    feedbackForm = $(".feedback_form form");
    textField = $(feedbackForm.find("input[type=text]"));
    textareaField = $(feedbackForm.find("textarea"));
    placeholderClass = ".placeholder";

    textField.focusin(function(){
        $(this).prev(placeholderClass).hide();
    });
    textField.focusout(function(){
        if ($(this).val() == "")
            $(this).prev(placeholderClass).show();
    });
    textField.prev(placeholderClass).click(function(){
        $(this).focus();
    });

    textareaField.focusin(function(){
        $(this).prev(placeholderClass).hide();
    });
    textareaField.focusout(function(){
        if ($(this).val() == "")
            $(this).prev(placeholderClass).show();
    });
    textareaField.prev(placeholderClass).click(function(){
        $(this).focus();
    });


    $(".feedback_form .stars li").mouseover(function(){
        if ($(this).hasClass("unactive_star_2_icon")) {
            $(this).removeClass("unactive_star_2_icon").addClass("star_2_icon");
        }
    });
    $(".feedback_form .stars li").mouseout(function(){
        if ($(this).hasClass("star_2_icon") && (!$(this).hasClass("active"))) {
            $(this).addClass("unactive_star_2_icon").removeClass("star_2_icon");
        }
    });

    $(".send_review").click(function(){
        if ($(".feedback_form").is(":visible")) {
            $(".feedback_form").hide();
        } else {
            $(".feedback_form").show();
        }
    });

    $("#school_info_navigation .navigation li").click(function(){
        if (!$(this).hasClass("active")) {
            $("#school_info_navigation li").removeClass("active");
            $("#mobile_photo_slider").show();
            $("#mobile_kurs").hide();
            $(this).addClass("active");
            $(".content_card li.item").removeClass("active");
            $($(".content_card li.item")[$(this).index()]).addClass("active");
        }
    });

    /*
    $("#mobil_kurs .button").click(function(){
        $("#mobile_kurs").hide();
        $("#mobile_photo_slider").show();
    });
    */

    $(".questions .navigation li").click(function(){
        if (!$(this).hasClass("active")) {
            $(".questions .navigation li").removeClass("active");
            $(this).addClass("active");
            $(".bars .bar").removeClass("active");
            $($(".bars .bar")[$(this).index()]).addClass("active");
        }
    });

}

function initializeQuestionForm() {
    feedbackForm = $(".question_form form");
    textField = $(feedbackForm.find("input[type=text]"));
    textareaField = $(feedbackForm.find("textarea"));
    placeholderClass = ".placeholder";

    textField.focusin(function(){
        $(this).prev(placeholderClass).hide();
    });
    textField.focusout(function(){
        if ($(this).val() == "")
            $(this).prev(placeholderClass).show();
    });
    textField.prev(placeholderClass).click(function(){
        $(this).focus();
    });

    textareaField.focusin(function(){
        $(this).prev(placeholderClass).hide();
    });
    textareaField.focusout(function(){
        if ($(this).val() == "")
            $(this).prev(placeholderClass).show();
    });
    textareaField.prev(placeholderClass).click(function(){
        $(this).focus();
    });

    $(".send_question").click(function(){
        if ($(".question_form").is(":visible")) {
            $(".question_form").hide();
        } else {
            $(".question_form").show();
        }
    });

}

function initializeMRequestForm() {
    feedbackForm = $(".recenze form");
    textField = $(feedbackForm.find("input[type=text]"));
    textareaField = $(feedbackForm.find("textarea"));
    placeholderClass = ".placeholder";

    textField.focusin(function(){
        $(this).prev(placeholderClass).hide();
    });
    textField.focusout(function(){
        if ($(this).val() == "")
            $(this).prev(placeholderClass).show();
    });
    textField.prev(placeholderClass).click(function(){
        $(this).focus();
    });

    textareaField.focusin(function(){
        $(this).prev(placeholderClass).hide();
    });
    textareaField.focusout(function(){
        if ($(this).val() == "")
            $(this).prev(placeholderClass).show();
    });
    textareaField.prev(placeholderClass).click(function(){
        $(this).focus();
    });

}

function initializeMQuestForm() {
    feedbackForm = $(".dotazy form");
    textField = $(feedbackForm.find("input[type=text]"));
    textareaField = $(feedbackForm.find("textarea"));
    placeholderClass = ".placeholder";

    textField.focusin(function(){
        $(this).prev(placeholderClass).hide();
    });
    textField.focusout(function(){
        if ($(this).val() == "")
            $(this).prev(placeholderClass).show();
    });
    textField.prev(placeholderClass).click(function(){
        $(this).focus();
    });

    textareaField.focusin(function(){
        $(this).prev(placeholderClass).hide();
    });
    textareaField.focusout(function(){
        if ($(this).val() == "")
            $(this).prev(placeholderClass).show();
    });
    textareaField.prev(placeholderClass).click(function(){
        $(this).focus();
    });

}

function initializeLocation() {
    $(".praha").click(function(){
        if (!$(this).hasClass("active"))  {
            $(".ceshla").removeClass("active");
            $("#ceshka_cnt").hide();
            $("#praha_cnt").show();
            $(this).addClass("active");
        }
    });
    $(".ceshla").click(function(){
        if (!$(this).hasClass("active"))  {
            $(".praha").removeClass("active");
            $("#praha_cnt").hide();
            $("#ceshka_cnt").show();
            $(this).addClass("active");
        }
    });
}


function initializeSchoolTable() {
    $(".popup_row").click(function(){
        if ($(".open_popup_row").is(":visible")) {
            $(this).find(".top_arrow_icon").removeClass("top_arrow_icon").addClass("bottom_arrow_icon");
            $(".open_popup_row").hide();
        } else {
            $(this).find(".bottom_arrow_icon").addClass("top_arrow_icon").removeClass("bottom_arrow_icon");
            $(".open_popup_row").show();
        }
    });
}

function initializePopup() {

    $(".popup").click(function(){
        $(this).hide();
    });

    $(".popup .popup_cnt").click(function(e){
        e.stopPropagation();
    });

    $(".nevite").click(function(){
        $("body").scrollTop(0);
        $("#stages_popup").show();
        return false;
    });

    $(".studia").click(function(){
        $("body").scrollTop(0);
        $("#quest_popup").show();
        return false;
    });

    $("table .button").click(function(){
        $("body").scrollTop(0);
        $("#kurs_popup").show();
        return false;
    });

    $(".close_cross").click(function(){
        $(".popup").hide();
    });

    $(".popup .button").click(function(){
        $(".popup").hide();
    });

    $(".cash .button").click(function() {
        $("#school_info_navigation .navigation li").removeClass("active");
        $("#mobile_kurs").show();
        $("#mobile_photo_slider").hide();
        $("#school_info_navigation .content_card .item").removeClass("active");
    });

    $("#mobile_review").click(function() {
        if ($(".recenze .form").is(":visible")) {
            $("#mobile_review .active").show();
            $("#mobile_review .icon").hide();
            $(".recenze .form").hide();
        } else {
            $("#mobile_review .active").hide();
            $("#mobile_review .icon").show();
            $(".recenze .form").show();
        }
    });

    $("#mobile_dotaz").click(function() {
        if ($(".dotazy .form").is(":visible")) {
            $("#mobile_dotaz .active").show();
            $("#mobile_dotaz .icon").hide();
            $(".dotazy .form").hide();
        } else {
            $("#mobile_dotaz .active").hide();
            $("#mobile_dotaz .icon").show();
            $(".dotazy .form").show();
        }
    });

}

function initializeCatalogFilter() {

    $(".checkbox_field").click(function(){
        checkbox = $(this).children("input[type=checkbox]");
        fake_checkbox = $(this).children(".fake_checkbox");
        if ($(this).hasClass("check")) {
            if ($(this).hasClass("plus_checkbox_field"))
                fake_checkbox.addClass("uncheck_icon").removeClass("plus_check_icon");
            else
                fake_checkbox.addClass("uncheck_icon").removeClass("check_icon");
            $(this).removeClass("check");
            checkbox.attr("checked", false);
        } else {
            if ($(this).hasClass("plus_checkbox_field"))
                fake_checkbox.removeClass("uncheck_icon").addClass("plus_check_icon");
            else
                fake_checkbox.removeClass("uncheck_icon").addClass("check_icon");
            $(this).addClass("check");
            checkbox.attr("checked", true);
        }
    });

}

function initializePriceSlider() {
    $( ".price_slider" ).slider({
        range: true,
        min: 0,
        max: 500,
        values: [ 100, 300 ],
        slide: function( event, ui ) {
            $(".current_price_of .value").text(ui.values[0]);
            $(".current_price_to .value").text(ui.values[1]);
            $("#price_of").val(ui.values[0]);
            $("#price_to").val(ui.values[1]);
        }
    });
}

function initializeCatalogNavigation() {

    $("#razeni_open").click(function(){
        if ($("#filter_open").hasClass("active")) {
            $("#school_list_filter").hide();
            $("#filter_open").removeClass("active");
        }
        if ($(this).hasClass("active")) {
            $("#razeni_filter").hide();
            $(this).removeClass("active");
        } else {
            $("#razeni_filter").show();
            $(this).addClass("active")
        }
    });

    $("#filter_open").click(function(){
        if ($("#razeni_open").hasClass("active")) {
            $("#razeni_filter").hide();
            $("#razeni_open").removeClass("active");
        }
        if ($(this).hasClass("active")) {
            $("#school_list_filter").hide();
            $(this).removeClass("active");
        } else {
            $("#school_list_filter").show();
            $(this).addClass("active")
        }
    });

}


function initializeMobileMenu() {
    mobileMenuBtn = $(".menu_button");
    mobileMenu = $("#mobile_menu");

    mobileMenuBtn.click(function(){
        if(mobileMenu.is(":visible")) {
            mobileMenuBtn.children(".icon").removeClass("cross_icon");
            mobileMenuBtn.children(".icon").addClass("menu_icon");
            mobileMenu.hide();
        }
        else {
            mobileMenuBtn.children(".icon").addClass("cross_icon");
            mobileMenuBtn.children(".icon").removeClass("menu_icon");
            mobileMenu.show();
        }
    });
}

function initializeQuestionsBar() {
    $(".questions .header").click(function(){
        parent = $(this).parent().parent();
        if($(this).parent().parent().hasClass("active")) {
            parent.removeClass("active");
            parent.find(".icon").removeClass("small_bottom_arrow_icon").addClass("small_left_arrow_icon");
        } else {
            parent.addClass("active");
            parent.find(".icon").removeClass("small_left_arrow_icon").addClass("small_bottom_arrow_icon");
        }
    });
}

function initializeQuestionsPlanks() {
    $("#landing_questions .question .title").click(function(){
        description = $(this).next();
        if(description.is(":visible")) {
            description.parent().removeClass("active");
            description.hide();
        }
        else {
            description.parent().addClass("active");
            description.show();
        }
    });
}
function initMobileSlider() {

    object = $("#mobile_photo_slider");
    children = object.children(".slider_children");
    $(children).children("li").first().addClass("current");
    reinitChildren();

    $("#mobile_photo_slider").children(".left_switcher").click(function(){
        previousSlide();
    });

    $("#mobile_photo_slider").children(".right_switcher").click(function(){
        nextSlide();
    });

    function nextSlide() {
        slideAnimation("right");
    }

    function previousSlide() {
        slideAnimation("left");
    }

    $("#mobile_photo_slider").find(".pagination li").click(function () {
        slideAnimationByIndex($(this).index());
    });

    function slideAnimation(type) {

        admixture = 0;
        current = $("#mobile_photo_slider .slider_children").children("li.current").index();
        length = $("#mobile_photo_slider .slider_children").children("li").length - 1;
        width = $("#mobile_photo_slider .slider_children").children("li.current").width();
        if (length > 0) {
            switch (type) {
                case "right":
                    if (current == length - admixture)
                        current = 0;
                    else
                        current = current + 1;
                    break;
                case "left":
                    if (current == 0)
                        current = length - admixture;
                    else
                        current = current - 1;
                    break;
            }

            $("#mobile_photo_slider .slider_children").animate({left: -( current * width )}, 300);
            $("#mobile_photo_slider .slider_children").children("li").removeClass("current");
            $($("#mobile_photo_slider .slider_children").children("li")[current]).addClass("current");
            $("#mobile_photo_slider").children(".pagination").find("li").removeClass("active");
            $($("#mobile_photo_slider").find(".pagination li")[current]).addClass("active");
        }
    }

    function slideAnimationByIndex(position) {
        length = $("#mobile_photo_slider .slider_children").children("li").length - 1;
        width = $("#mobile_photo_slider .slider_children").children("li.current").width();
        $("#mobile_photo_slider .slider_children").animate({left: -( position * width )}, 300);
        $($("#mobile_photo_slider .slider_children").children("li")).removeClass("current");
        $($("#mobile_photo_slider .slider_children").children("li")[position]).addClass("current");
        $("#mobile_photo_slider").children(".pagination").children("li").removeClass("active");
        $($("#mobile_photo_slider").children(".pagination").children("li")[position]).addClass("active");
    }

    function reinitChildren() {
        width = $("#mobile_photo_slider .slider_children").children("li.current").width();
        current = $($("#mobile_photo_slider .slider_children").children("li.current")).index();
        $("#mobile_photo_slider .slider_children").children("li").width($(window).width());
        $("#mobile_photo_slider .slider_children").css("left", -( current * width ));
    }

    $(window).resize(function(){
        reinitChildren();
    });
}
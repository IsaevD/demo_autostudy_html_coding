function initPhotoSlider(id) {

    children = $("#photo_slider .slider_children");
    $(children).children("li").first().addClass("current");

    $($("#photo_slider").children(".left_switcher")).click(function(){
        previousSlide();
    });

    $($("#photo_slider").children(".right_switcher")).click(function(){
        nextSlide();
    });

    function nextSlide() {
        slideAnimation("right");
    }

    function previousSlide() {
        slideAnimation("left");
    }

    $("#photo_slider").find(".pagination li").click(function () {
        slideAnimationByIndex($(this).index());
    });

    function slideAnimation(type) {
        admixture = 0;
        current = $($("#photo_slider .slider_children").children("li.current")).index();
        length = $("#photo_slider .slider_children").children("li").length - 1;
        width = $("#photo_slider .slider_children").children("li.current").width();
        if (length > 0) {
            switch (type) {
                case "right":
                    if (current == length - admixture)
                        current = 0;
                    else
                        current = current + 1;
                    break;
                case "left":
                    if (current == 0)
                        current = length - admixture;
                    else
                        current = current - 1;
                    break;
            }

            $("#photo_slider .slider_children").animate({left: -( current * width )}, 300);
            $($("#photo_slider .slider_children").children("li")).removeClass("current");
            $($("#photo_slider .slider_children").children("li")[current]).addClass("current");
            $("#photo_slider").children(".pagination").find("li").removeClass("active");
            $($("#photo_slider").find(".pagination li")[current]).addClass("active");
        }
    }

    function slideAnimationByIndex(position) {
        length = $("#photo_slider .slider_children").children("li").length - 1;
        width = $("#photo_slider .slider_children").children("li.current").width();
        $("#photo_slider .slider_children").animate({left: -( position * width )}, 300);
        $($("#photo_slider .slider_children").children("li")).removeClass("current");
        $($("#photo_slider .slider_children").children("li")[position]).addClass("current");
        $("#photo_slider").children(".pagination").children("li").removeClass("active");
        $($("#photo_slider").children(".pagination").children("li")[position]).addClass("active");
    }

}